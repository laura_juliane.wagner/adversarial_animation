using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject prefab;

    
    public float spawnInterval = 1.0f;


    void Start()
    {
        InvokeRepeating("spawnObject", spawnInterval, spawnInterval);        
    }

    void spawnObject()
    {
    Instantiate(prefab, transform.position, transform.rotation);
    }
}
